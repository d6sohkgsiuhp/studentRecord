-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2017 at 07:01 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trainee`
--

-- --------------------------------------------------------

--
-- Table structure for table `check_list_stu`
--

CREATE TABLE `check_list_stu` (
  `lis_id` int(11) NOT NULL,
  `lis_com_id` int(12) NOT NULL,
  `lis_stu_id` int(12) NOT NULL,
  `lis_check` varchar(45) NOT NULL,
  `lis_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lis_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `check_list_stu`
--

INSERT INTO `check_list_stu` (`lis_id`, `lis_com_id`, `lis_stu_id`, `lis_check`, `lis_time`, `lis_note`) VALUES
(0, 2, 3, 'Come', '2017-02-10 03:36:34', ''),
(0, 2, 3, 'Come', '2017-02-10 08:18:41', '');

-- --------------------------------------------------------

--
-- Table structure for table `check_student`
--

CREATE TABLE `check_student` (
  `che_id` int(11) NOT NULL,
  `che_company_id` int(11) NOT NULL,
  `che_student` varchar(45) NOT NULL,
  `che_position` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `com_id` int(11) NOT NULL,
  `com_username` varchar(12) NOT NULL,
  `com_password` varchar(12) NOT NULL,
  `com_name` varchar(20) NOT NULL,
  `com_address` text NOT NULL,
  `com_web` varchar(20) NOT NULL,
  `com_guid` varchar(20) NOT NULL,
  `com_phone` varchar(11) NOT NULL,
  `com_email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

CREATE TABLE `enroll` (
  `enr_id` int(11) NOT NULL,
  `enr_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enr_enroll` text NOT NULL,
  `enr_comment` text NOT NULL,
  `enr_stu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_login`
--

CREATE TABLE `log_login` (
  `log_login_id` int(11) NOT NULL,
  `log_login_name` varchar(100) NOT NULL,
  `log_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_login_status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name_university`
--

CREATE TABLE `name_university` (
  `nam_id` int(11) NOT NULL,
  `nam_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `name_university`
--

INSERT INTO `name_university` (`nam_id`, `nam_name`) VALUES
(1, 'จุฬาลงกรณ์มหาวิทยาลัย'),
(2, 'มหาวิทยาลัยเกษตรศาสตร์'),
(3, 'มหาวิทยาลัยธรรมศาสตร์'),
(4, 'มหาวิทยาลัยศรีนครินวิโรฒ'),
(5, 'สถาบันบัณฑิตพัฒนบริหารศาสตร์'),
(6, 'สถาบันเทคโนโลยีพระจอมเกล้าพระนครเหนือ'),
(7, 'สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง'),
(8, 'มหาวิทยาลัยมหิดล'),
(9, 'มหาวิทยาลัยศิลปากร'),
(10, 'มหาวิทยาลัยบูรพา'),
(11, 'มหาวิทยาลัยนเรศวร'),
(12, ' มหาวิทยาลัยแม่โจ้'),
(13, 'มหาวิทยาลัยเชียงใหม่'),
(14, 'มหาวิทยาลัยขอนแก่น'),
(15, 'มหาวิทยาลัยอุบลราชธานี'),
(16, 'มหาวิทยาลัยมหาสารคาม'),
(17, 'มหาวิทยาลัยสงขลานครินทร์'),
(18, 'มหาวิทยาลัยทักษิณ'),
(19, 'มหาวิทยาลัยนราธิวาสราชนครินทร์'),
(20, 'มหาวิทยาลัยนครพนม'),
(21, 'มหาวิทยาลัยเทคโนโลยีสุรนารี'),
(22, 'มหาวิทยาลัยวลัยลักษณ์'),
(23, 'มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี'),
(24, 'มหาวิทยาลัยแม่ฟ้าหลวง'),
(25, 'มหาวิทยาลัยมหามกุฏราชวิทยาลัย'),
(26, 'มหาวิทยาลัยมหาจุฬาลงกรณ์ราชวิทยาลัย'),
(27, 'มหาวิทยาลัยราชภัฏเชียงราย'),
(28, 'มหาวิทยาลัยราชภัฏเชียงใหม่'),
(29, 'มหาวิทยาลัยราชภัฏลำปาง'),
(30, 'มหาวิทยาลัยราชภัฏอุตรดิตถ์'),
(31, 'มหาวิทยาลัยราชภัฏกำแพงเพชร'),
(32, 'มหาวิทยาลัยราชภัฏนครสวรรค์'),
(33, 'มหาวิทยาลัยราชภัฏพิบูลสงคราม'),
(34, 'มหาวิทยาลัยราชภัฏเพชรบูรณ์'),
(35, 'มหาวิทยาลัยราชภัฏเลย'),
(36, 'มหาวิทยาลัยราชภัฏสกลนคร'),
(37, 'มหาวิทยาลัยราชภัฏอุดรธานี'),
(38, 'มหาวิทยาลัยราชภัฏมหาสารคาม'),
(39, 'มหาวิทยาลัยราชภัฏนครราชสีมา'),
(40, 'มหาวิทยาลัยราชภัฏบุรีรัมย์'),
(41, 'มหาวิทยาลัยราชภัฏสุรินทร์'),
(42, 'มหาวิทยาลัยราชภัฏอุบลราชธานี'),
(43, 'มหาวิทยาลัยราชภัฏศรีสะเกษ'),
(44, 'มหาวิทยาลัยราชภัฏชัยภูมิ'),
(45, 'มหาวิทยาลัยราชภัฏร้อยเอ็ด'),
(46, 'มหาวิทยาลัยราชภัฏราชนครินทร์'),
(47, 'มหาวิทยาลัยราชภัฏเทพสตรี'),
(48, 'มหาวิทยาลัยราชภัฏพระนครศรีอยูธยา'),
(49, 'มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์'),
(50, 'มหาวิทยาลัยราชภัฏรำไพพรรณี'),
(51, 'มหาวิทยาลัยราชภัฏกาญจนบุรี'),
(52, 'มหาวิทยาลัยราชภัฏนครปฐม'),
(53, 'มหาวิทยาลัยราชภัฏเพชรบุรี'),
(54, 'มหาวิทยาลัยราชภัฏหมู่บ้านจอมบึง'),
(55, 'มหาวิทยาลัยราชภัฏภูเก็ต'),
(56, 'มหาวิทยาลัยราชภัฏยะลา'),
(57, 'มหาวิทยาลัยราชภัฏสงขลา'),
(58, 'มหาวิทยาลัยราชภัฏนครศรีธรรมราช'),
(59, 'มหาวิทยาลัยราชภัฏสุราษฏร์ธานี'),
(60, 'มหาวิทยาลัยราชภัฏจันทรเกษม'),
(61, 'มหาวิทยาลัยราชภัฏธนบุรี'),
(62, 'มหาวิทยาลัยราชภัฏบ้านสมเด็จเจ้าพระยา'),
(63, 'มหาวิทยาลัยราชภัฏพระนคร'),
(64, 'มหาวิทยาลัยราชภัฏสวนดุสิต'),
(65, 'มหาวิทยาลัยราชภัฏสวนสุนันทา'),
(66, 'มหาวิทยาลัยเทคโนโลยีราชมงคลธัญบุรี'),
(67, 'มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ'),
(68, 'มหาวิทยาลัยเทคโนโลยีราชมงคลตะวันออก'),
(69, 'มหาวิทยาลัยเทคโนโลยีราชมงคลพระนคร'),
(70, 'มหาวิทยาลัยเทคโนโลยีราชมงคลศรีวิชัย'),
(71, 'มหาวิทยาลัยเทคโนโลยีราชมงคลล้านนา'),
(72, 'มหาวิทยาลัยเทคโนโลยีราชมงคลสุวรรณภูมิ'),
(73, 'มหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์'),
(74, 'มหาวิทยาลัยเทคโนโลยีราชมงคลอีสาน'),
(75, 'สถาบันเทคโนโลยีปทุมวัน'),
(76, 'มหาวิทยาลัยกรุงเทพ'),
(77, 'มหาวิทยาลัยเกษมบัณฑิต'),
(78, 'มหาวิทยาลัยธุรกิจบัณฑิตย์'),
(79, 'มหาวิทยาลัยเกริก'),
(80, 'มหาวิทยาลัยเซนต์จอห์น'),
(81, 'มหาวิทยาลัยเทคโนโลยีมหานคร'),
(82, 'มหาวิทยาลัยศรีปทุม'),
(83, 'มหาวิทยาลัยหอการค้าไทย'),
(84, 'มหาวิทยาลัยอัสสัมชัญ'),
(85, 'มหาวิทยาลัยเอเชียอาคเนย์'),
(86, 'มหาวิทยาลัยรัตนบัณฑิต'),
(87, 'มหาวิทยาลัยสยาม'),
(88, 'มหาวิทยาลัยอีสเทิร์นเอเชีย'),
(89, 'มหาวิทยาลัยปทุมธานี'),
(90, 'มหาวิทยาลัยชินวัตร'),
(91, 'มหาวิทยาลัยรังสิต'),
(92, 'มหาวิทยาลัยเว็บสเตอร์ (ประเทศไทย)'),
(93, 'มหาวิทยาลัยนานาชาติ แสตมฟอร์ด'),
(94, 'มหาวิทยาลัยหัวเฉียวเฉลิมพระเกียรติ'),
(95, 'มหาวิทยาลัยคริสเตียน'),
(96, 'มหาวิทยาลัยเวสเทิร์น'),
(97, 'มหาวิทยาลัยภาคกลาง'),
(98, 'มหาวิทยาลัยพายัพ'),
(99, 'มหาวิทยาลัยเจ้าพระยา'),
(100, 'มหาวิทยาลัยนอร์ท-เชียงใหม่'),
(101, 'มหาวิทยาลัยภาคตะวันออกเฉียงเหนือ'),
(102, 'มหาวิทยาลัยวงษ์ชวลิตกุล'),
(103, 'มหาวิทยาลัยราชธานี'),
(104, 'มหาวิทยาลัยหาดใหญ่'),
(105, 'มหาวิทยาลัยวิทยาศาสตร์และเทคโนโลยีแห่งเอเชีย'),
(106, 'วิทยาลัยดุสิตธานี'),
(107, 'วิทยาลัยทองสุข'),
(108, 'วิทยาลัยเซนต์หลุยส์'),
(109, 'วิทยาลัยมิชชั่น'),
(110, 'วิทยาลัยรัชต์ภาคย์'),
(111, 'วิทยาลัยราชพฤกษ์'),
(112, 'วิทยาลัยแสงธรรม'),
(113, 'วิทยาลัยเทคโนโลยีธนบุรี'),
(114, 'วิทยาลัยเซาธ์อีสบางกอก '),
(115, 'วิทยาลัยนอร์ทกรุงเทพ '),
(116, 'วิทยาลัยเซนต์เทเรซา – อินติ'),
(117, 'วิทยาลัยกรุงเทพธนบุรี'),
(118, 'วิทยาลัยเทคโนโลยีราชธานีอุดร '),
(119, 'วิทยาลัยสันตพล '),
(120, 'วิทยาลัยโปลีเทคนิคภาคตะวันออกเฉียงเหนือ'),
(121, 'วิทยาลัยบัณฑิตเอเชีย '),
(122, 'วิทยาลัยบัณฑิตบริหารธุรกิจ'),
(123, 'วิทยาลัยนครราชสีมา'),
(124, 'วิทยาลัยเฉลิมกาญจนา'),
(125, 'วิทยาลัยเทคโนโลยีภาคใต้'),
(126, 'วิทยาลัยศรีโสภณ'),
(127, 'วิทยาลัยตาปี'),
(128, 'มหาวิทยาลัยอิสลามยะลา'),
(129, 'วิทยาลัยพุทธศาสนานานาชาติ'),
(130, 'วิทยาลัยโยนก'),
(131, 'วิทยาลัยลุ่มน้ำปิง'),
(132, 'วิทยาลัยพิษณุโลก'),
(133, 'วิทยาลัยฟาร์อีสเทอร์น'),
(134, 'วิทยาลัยเชียงราย'),
(135, 'วิทยาลัยอินเตอร์เทค-ลำปาง'),
(136, 'วิทยาลัยเทคโนโลยีสยาม');

-- --------------------------------------------------------

--
-- Table structure for table `position_create`
--

CREATE TABLE `position_create` (
  `pos_id` int(11) NOT NULL,
  `pos_name` varchar(50) NOT NULL,
  `com_id_position` varchar(50) NOT NULL,
  `pos_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `position_student`
--

CREATE TABLE `position_student` (
  `position_stu_number` int(11) NOT NULL,
  `stu_id` int(11) NOT NULL,
  `position_stu_name` varchar(30) NOT NULL,
  `position_stu_company` varchar(30) NOT NULL,
  `stu_status` varchar(10) NOT NULL,
  `position_stu_starttime` date NOT NULL,
  `position_stu_endtime` date NOT NULL,
  `position_stu_regis` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stu_id` int(11) NOT NULL,
  `stu_username` varchar(12) NOT NULL,
  `stu_password` varchar(12) NOT NULL,
  `stu_name` varchar(20) NOT NULL,
  `stu_address` text NOT NULL,
  `stu_birthday` date NOT NULL,
  `stu_university` varchar(50) NOT NULL,
  `stu_number` varchar(30) NOT NULL,
  `stu_branch` varchar(30) NOT NULL,
  `stu_phone` varchar(20) NOT NULL,
  `stu_email` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE `university` (
  `uni_id` int(11) NOT NULL,
  `uni_username` varchar(12) NOT NULL,
  `uni_password` varchar(12) NOT NULL,
  `uni_teacher` varchar(45) NOT NULL,
  `uni_name` varchar(100) NOT NULL,
  `uni_address` text NOT NULL,
  `uni_web` varchar(45) NOT NULL,
  `uni_email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `check_student`
--
ALTER TABLE `check_student`
  ADD PRIMARY KEY (`che_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `enroll`
--
ALTER TABLE `enroll`
  ADD PRIMARY KEY (`enr_id`);

--
-- Indexes for table `log_login`
--
ALTER TABLE `log_login`
  ADD PRIMARY KEY (`log_login_id`);

--
-- Indexes for table `name_university`
--
ALTER TABLE `name_university`
  ADD PRIMARY KEY (`nam_id`);

--
-- Indexes for table `position_create`
--
ALTER TABLE `position_create`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `position_student`
--
ALTER TABLE `position_student`
  ADD PRIMARY KEY (`position_stu_number`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stu_id`);

--
-- Indexes for table `university`
--
ALTER TABLE `university`
  ADD PRIMARY KEY (`uni_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `check_student`
--
ALTER TABLE `check_student`
  MODIFY `che_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `enroll`
--
ALTER TABLE `enroll`
  MODIFY `enr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_login`
--
ALTER TABLE `log_login`
  MODIFY `log_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1227;
--
-- AUTO_INCREMENT for table `name_university`
--
ALTER TABLE `name_university`
  MODIFY `nam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `position_create`
--
ALTER TABLE `position_create`
  MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `position_student`
--
ALTER TABLE `position_student`
  MODIFY `position_stu_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `stu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `university`
--
ALTER TABLE `university`
  MODIFY `uni_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
