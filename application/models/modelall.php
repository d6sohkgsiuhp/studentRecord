<?php
class Modelall extends CI_MODEL
{
    public function __construct(){
       
        parent::__construct();
    }
    public function getAll(){

    }
        // ลงทะเบียนนักศึกษา
    public function insertstudent($data){
    
        $this->db->insert('student', $data);
    }
        // ลงทะเบียนบริษัท
    public function insertcompany($data){
    
        $this->db->insert('company', $data);
    }
    // ลงทะเบียนมหาวิทยาลัย
    public function insertuniversity($data){
     
        $this->db->insert('university',$data);
    }
    // เช็ค log ที่  login เข้ามา
    public function insertlog($data){
     
        $this->db->insert('log_login',$data);
    }
    // สร้างตำแหน่งงาน
    public function insertposition($data){
    
         $this->db->insert('position_create', $data);
             echo"<script language=\"JavaScript\">";
             echo"alert('สร้างตำแหน่งสำเร็จแล้ว')";
             echo"</script>";
    }
    // สมัครตำแหน่งของนักศึกษา
    public function insertstudent_position($data){
    
       $this->db->insert('position_student',$data);
    }
    //ดึงข้อมูล student ไปแก้ไข
    public function get_student($id){ 
    
        $this->db->where('stu_id', $id);
        $this->db->from('student');
        $data = $this->db->get();
        return $data->result();
    }
    // update ข้อมูล student
    public function update_student($data){
    
        $this->db->where('stu_id', $data['id']); 
        $this->db->update('student', $data['update']); 
    }
    //ดึงข้อมูล company ไปแก้ไข
   public function get_company($id){
    
        $this->db->where('com_id', $id);
        $this->db->from('company');
        $data = $this->db->get();
        return $data->result();
    }
    //update ข้อมูล company
    public function update_company($data){
               
        $this->db->where('com_id', $data['id']); 
        $this->db->update('company', $data['update']); 
    }
    // ดึงข้อมูล university ไปแก้ไข
    public function get_university($id){

        $this->db->where('uni_id', $id);
        $this->db->from('university');
        $data = $this->db->get();
        return $data->result();
    }
    // update ข้อมูล  university
    public function update_university($data){   
    
        $this->db->where('uni_id', $data['id']); 
        $this->db->update('university', $data['update']); 
    }
    public function get_dropdown_list(){
     
        $query = $this->db->query('SELECT pos_name FROM position_create');
        return $query->result();
    }
    // บันทึกนักศึกษา  ใหม่
    public function edit_notes_student($data){    
    
        $this->db->insert('enroll',$data);
            echo"<script language=\"JavaScript\">";
            echo"alert('บันทึกสำเร็จแล้ว')";
            echo"</script>";
    
    }
   // table position_create  ดึงข้อมูล  ตำแหน่ง  join  com_id_position  กับ com_id  
    public function list_position(){
        
        $this->db->from('position_create');
        $this->db->join('company','position_create.com_id_position = company.com_id');
        $query = $this->db->get();
         return $query->result();
    }
    // table position_student ดึงข้อมูล ที่นักศึกษาสมัครเข้า  join  position_stu_company  กับ  com_id  
    public function list_status($id) {
    
        $this->db->where('stu_id', $id);
        $this->db->like('stu_status',1);
        $this->db->from('position_student');
        $this->db->join('company','position_student.position_stu_company = company.com_id');
        $this->db->join('position_create','position_student.position_stu_name = position_create.pos_id');
        $data = $this->db->get();
	    return $data->result();
    }
// table position_student ดึงข้อมูล  นักศึกษาที่สมัครเข้าบริษัท  join  position_stu_company  กับ  com_id 
    public function list_RegisPosition($id){
    
        $this->db->where('position_stu_company', $id); 
        $this->db->like('stu_status',1);
        $this->db->from('position_student');
        $this->db->join('student','position_student.stu_id = student.stu_id');
        $this->db->join('position_create','position_student.position_stu_name = position_create.pos_id ');
        $data = $this->db->get();
	    return $data->result();
    }
    //position_create  แก้ไขข้อมูล ตำแหน่งท่ บริษัทสร้าง  join  com_id_position  กับ  com_id
    public function edit_position($id){
        
        $this->db->where('com_id', $id); 
        $this->db->from('position_create');
        $this->db->join('company','position_create.com_id_position = company.com_id');
        $data = $this->db->get();
	    return $data->result();
    }
    // ลบการสมัครตำแหน่ง
    public function deleted_regisposition($id){
        
        $this->db->delete('position_student', array('position_stu_number' => $id)); 
    }
    // บริษัทยืนยันการสมัครนักศึกษา
    public function verify_regisposition($data){
        
         $this->db->insert('check_student', $data);
         $status = $this->db->get('position_student');
         return $this->db->insert_id();
    }
    
    public function update_status($status){
       
        $this->db->where('stu_id', $status['id']); 
        $this->db->update('position_student', $status['update']); 
    }
    // บริษัทลบตำแหน่ง
    public function deleted_position($id){
        
        $this->db->delete('position_create', array('pos_id' => $id));
        
    }
    // แก้ไขตำแหน่งที่สมัคร
    public function get_position($id){
        $this->db->where('pos_id', $id); 
        $this->db->from('position_create');
        $data = $this->db->get();
	    return $data->result();
    }
    public function list_student_com($id)
    {
        $this->db->where('che_company_id', $id); 
        $this->db->from('check_student');
        $this->db->join('position_create','check_student.che_position = position_create.pos_id');
        $this->db->join('student','check_student.che_student = student.stu_id');
        $data = $this->db->get();
	    return $data->result();
    }
    public function get_name_university($name)
    {
        // $this->db->where('uni_name', $name); 
        $this->db->like('nam_name',$name);
        $this->db->from('name_university');
        $data = $this->db->get();
	    return $data->result();
    }
    public function choose_university($id)
    {
        $this->db->where('nam_id', $id); 
        $this->db->from('name_university');
        $data = $this->db->get();
	    return $data->result();
    }
    //  เรียกบันทึกนักศึกษาทั้งหมด  
    public function all_stu_enroll($id){
        $this->db->where('enr_stu_id', $id);
        $this->db->from('enroll');
        $this->db->join('student','enroll.enr_stu_id =student.stu_id');
        $query = $this->db->get();
        return $query->result();
    }
     //  เรียกบันทึกนักศึกษาทั้งหมด บริษัท  ----------------------------------------------------------------------------------
    public function all_stu_enroll_com($id){
        $this->db->where('enr_stu_id', $id);
        $this->db->from('enroll');
        $this->db->join('student','enroll.enr_stu_id =student.stu_id');
        $query = $this->db->get();
        return $query->result();
    }
    // เรียกดูชื่อนักศึกษาที่อยู่ในมหาลัย
    public function list_stu_university($id)  
    {
        $this->db->where('stu_university',$id);
        $this->db->from('check_student');
        $this->db->join('student','check_student.che_student =student.stu_id');
        $this->db->join('position_create','check_student.che_position =position_create.pos_id');
        $this->db->join('company','check_student.che_company_id =company.com_id');
        $data = $this->db->get();
        return $data->result();
    }
    // ดึงหน้าบันทึกของนักศึก  หน้า read_note
    public function read_note($id)
    {
        $this->db->where('enr_stu_id',$id);
        $this->db->from('enroll');
        $data = $this->db->get();
        return $data->result();
    }
    // เช็คชื่อนักศึกษาในหน้าของบริษัท 
    public function check_student_com($id)
    {
        $this->db->where('che_company_id',$id);
        $this->db->from('check_student');
        $this->db->join('student', 'check_student.che_student = student.stu_id');
        $this->db->join('position_create','check_student.che_position = position_create.pos_id');
        $data = $this->db->get();
        return $data->result();
    }
    //
    public function list_check_student($data)
    {
        // $this->db->from('check_list_stu');
        // $this->db->join();
        $this->db->insert('check_list_stu',$data);
    }
}