<?php



class Controller extends CI_CONTROLLER{
   
    public function __construct() {
        parent::__construct();
        // error_reporting(E_ALL);
        $this->load->model("modelall");
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('modelall','shop');
        $this->load->library('email');
        $this->load->library('cart');
        
    }

    public function index(){
        $this->load->view('index/first');
       
    }
    // first go login
    public function login_student(){
        $this->load->view('student/login_student');
    }
    // first go login
    public function login_university(){
       $this->load->view('university/login_university');
    }
    // first go login
        public function login_company(){
        $this->load->view('company/login_company');
    }
     //login_student go to  regis_student 
        public function createstudent(){
        $this->load->view('student/stu_choose_university');
    }
    
    // regis student
    public function inputstudent()                  {
        
        $this->form_validation->set_rules('stu_username', 'stu_username', 'required');	// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_password', 'stu_password', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_name', 'stu_name', 'required');			// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_address', 'stu_address', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_birthday', 'stu_birthday', 'required');	// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_university', 'stu_university', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_number', 'stu_number', 'required');			// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_branch', 'stu_branch', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_phone', 'stu_phone', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_email', 'stu_email', 'required');	// ดัก ช่องว่าง
    
    if ($this->form_validation->run() == FALSE){
				$this->login_student();
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";

              
    }else{

        $stu_username = $this->input->post('stu_username');
        $stu_password = $this->input->post('stu_password');
        $stu_name     = $this->input->post('stu_name');
        $stu_address  = $this->input->post('stu_address');
        $stu_birthday = $this->input->post('stu_birthday');
        $stu_university = $this->input->post('stu_university');
        $stu_number   = $this->input->post('stu_number');
        $stu_branch   = $this->input->post('stu_branch');
        $stu_phone    = $this->input->post('stu_phone');
        $stu_email    = $this->input->post('stu_email');
            $data = array (
                'stu_username'  => $stu_username,
                'stu_password'  => $stu_password,
                'stu_name'      => $stu_name,
                'stu_address'   => $stu_address,
                'stu_birthday'  => $stu_birthday,
                'stu_university'=> $stu_university,
                'stu_number'    => $stu_number,
                'stu_branch'    => $stu_branch,
                'stu_phone'     => $stu_phone,
                'stu_email'     => $stu_email
            );

            $this->modelall->insertstudent($data);
            $this->load->view('student/login_student');
        }


           // จากหน้า login_company submit  สมัครสมาชิก
    }
      //login_company go to  regis_company
    public function createcompany(){
        $this->load->view("company/regis_company");
    }
    //register company
    public function inputcompany() {
        
        $this->form_validation->set_rules('com_username', 'com_username', 'required');	
		$this->form_validation->set_rules('com_password', 'com_password', 'required');		
		$this->form_validation->set_rules('com_name', 'com_name', 'required');			
		$this->form_validation->set_rules('com_address', 'com_address', 'required');	
        $this->form_validation->set_rules('com_web', 'com_web', 'required');	
		$this->form_validation->set_rules('com_guid', 'com_guid', 'required');		
		$this->form_validation->set_rules('com_phone', 'com_phone', 'required');			
		$this->form_validation->set_rules('com_email', 'com_email', 'required');	

        if ($this->form_validation->run() == FALSE){
				$this->load->view('company/regis_company');
                        echo"<script language=\"JavaScript\">";
                        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
                        echo"</script>";
              
        }else{

        $com_username = $this->input->post('com_username');
        $com_password = $this->input->post('com_password');
        $com_name     = $this->input->post('com_name');
        $com_address  = $this->input->post('com_address');
        $com_web      = $this->input->post('com_web');
        $com_guid     = $this->input->post('com_guid');
        $com_phone    = $this->input->post('com_phone');
        $com_email    = $this->input->post('com_email');
            $data = array (
                'com_username'  => $com_username,
                'com_password'  => $com_password,
                'com_name'      => $com_name,
                'com_address'   => $com_address,
                'com_web'       => $com_web,
                'com_guid'      => $com_guid,
                'com_phone'     => $com_phone,
                'com_email'     => $com_email
            );

            $this->modelall->insertcompany($data);
            $this->load->view('company/login_company');
        }

            // จากหน้า login_university submit  สมัครสมาชิก

    }
     //login_university go to  regis_university
    public function createuniversity(){
       $this->load->view('university/uni_choose_university');
    }
    // regis university
    public function inputuniversity(){
        
        $this->form_validation->set_rules('uni_username', 'uni_username', 'required');	
		$this->form_validation->set_rules('uni_password', 'uni_password', 'required');		
		$this->form_validation->set_rules('uni_name', 'uni_name', 'required');
        $this->form_validation->set_rules('uni_teacher', 'uni_teacher', 'required');
		$this->form_validation->set_rules('uni_address', 'uni_address', 'required');	
        $this->form_validation->set_rules('uni_web', 'uni_web', 'required');	
		$this->form_validation->set_rules('uni_email', 'uni_email', 'required');		
        if ($this->form_validation->run() == FALSE){
				$this->login_university();
                        echo"<script language=\"JavaScript\">";
                        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
                        echo"</script>";
              
        }else{

        $uni_username = $this->input->post('uni_username');
        $uni_password = $this->input->post('uni_password');
        $uni_name     = $this->input->post('uni_name');
        $uni_teacher     = $this->input->post('uni_teacher');
        $uni_address  = $this->input->post('uni_address');
        $uni_web      = $this->input->post('uni_web');
        $uni_email    = $this->input->post('uni_email');

            $data = array (
                'uni_username'  => $uni_username,
                'uni_password'  => $uni_password,
                'uni_name'      => $uni_name,
                'uni_teacher'      => $uni_teacher,
                'uni_address'   => $uni_address,
                'uni_web'       => $uni_web,
                'uni_email'     => $uni_email,

            );

            $this->modelall->insertuniversity($data);
            $this->load->view('university/login_university');
        }
    }

   
    
    // เช็ค login student
    public function check_student(){          
        
        if($post = $this->input->post()){

                 $username     = $this->input->post('username');  //เก็บ login_student {
                 $student     = $this->input->post('student');
                 $data = array(
                  'log_login_name'      => $username,
                  'log_login_status'      => $student,
              );
              $this->modelall->insertlog($data);       
                        
            
            extract($post);
            $sql = "SELECT * FROM student where stu_username ='$username'and stu_password = '$password' ";
            $ret = $this->db->query($sql);
            if($ret->num_rows()){
                $data_ret = $ret->result();
                $this->session->set_userdata('stu_id',$data_ret[0]->stu_id);
                $this->session->set_userdata('stu_name',$data_ret[0]->stu_name);
                $this->session->set_userdata('stu_birthday',$data_ret[0]->stu_birthday);
                $this->session->set_userdata('stu_address',$data_ret[0]->stu_address);
                $this->session->set_userdata('stu_university',$data_ret[0]->stu_university);
                $this->session->set_userdata('stu_number',$data_ret[0]->stu_number);
                $this->session->set_userdata('stu_branch',$data_ret[0]->stu_branch);
                $this->session->set_userdata('stu_phone',$data_ret[0]->stu_phone);
                $this->session->set_userdata('stu_email',$data_ret[0]->stu_email);
                
            
                $this->load->view('student/Student_main');

                         // dropdown


            }else{
                 echo"<script language=\"JavaScript\">";
                 echo"alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')";
                 echo"</script>";
                 
                 $this->load->view('student/login_student');
                 }
        }  
    }

        // เช็ค login_company
        public function check_company(){          
        
            if($post = $this->input->post()){
               
                 $username     = $this->input->post('username');    //เก็บ login_company {
                 $company     = $this->input->post('company');
                 $data = array(
                  'log_login_name'      => $username,
                  'log_login_status'      => $company,
  
              );
              $this->modelall->insertlog($data);   
                                              //}
            
            extract($post);
            $sql = "SELECT * FROM company where com_username ='$username'and com_password = '$password' ";
            $ret = $this->db->query($sql);
            if($ret->num_rows()){
                $data_ret = $ret->result();
                $this->session->set_userdata('com_id',$data_ret[0]->com_id);
                $this->session->set_userdata('com_name',$data_ret[0]->com_name);
                $this->session->set_userdata('com_address',$data_ret[0]->com_address);
                $this->session->set_userdata('com_web',$data_ret[0]->com_web);
                $this->session->set_userdata('com_guid',$data_ret[0]->com_guid);
                $this->session->set_userdata('com_phone',$data_ret[0]->com_phone);
                $this->session->set_userdata('com_email',$data_ret[0]->com_email);
                
                $this->load->view('company/company_main');
            }else{
                 echo"<script language=\"JavaScript\">";
                 echo"alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')";
                 echo"</script>";
                 
                
               
                 $this->load->view('company/login_company');
                 }
            }  
    }

        // เช็ค login_university
    public function check_university(){
         
         if($post = $this->input->post()){

                $username     = $this->input->post('username');         //เก็บ login_company {
                 $university     = $this->input->post('university');
                 $data = array(
                  'log_login_name'      => $username,
                  'log_login_status'      => $university,
              );
              $this->modelall->insertlog($data);                                        //เก็บ login_company {
           
            extract($post);
            $sql = "SELECT * FROM university where uni_username ='$username'and uni_password = '$password' ";
            $ret = $this->db->query($sql);
            if($ret->num_rows()){
                $data_ret = $ret->result();
                $this->session->set_userdata('uni_id',$data_ret[0]->uni_id);
                $this->session->set_userdata('uni_name',$data_ret[0]->uni_name);
                $this->session->set_userdata('uni_teacher',$data_ret[0]->uni_teacher);
                $this->session->set_userdata('uni_address',$data_ret[0]->uni_address);
                $this->session->set_userdata('uni_web',$data_ret[0]->uni_web);
                $this->session->set_userdata('uni_email',$data_ret[0]->uni_email);
                        

                $this->load->view('university/university_main');
            }else{
                 echo"<script language=\"JavaScript\">";
                 echo"alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')";
                 echo"</script>";
                 
                 $this->load->view('university/login_university');
                 }
        }  
    }

        // แก้ไขข้อมูลนักศึกษา
public function edit_student($ids=''){
        $id = $this->input->post('id');              //$ids=''
        if (!empty($ids)) {
			$ret = $this->modelall->get_student($ids);
            return $ret; 
    }else{
			$ret = $this->modelall->get_student($id);
         }
        
		$data = array(
		"dataview" =>array(
					'stu_id'         => $ret[0]->stu_id,
                    'stu_password'   => $ret[0]->stu_password,
					'stu_name'       => $ret[0]->stu_name,
					'stu_address'    => $ret[0]->stu_address,
					'stu_birthday'   => $ret[0]->stu_birthday,
                    'stu_university' => $ret[0]->stu_university,
                    'stu_number'     => $ret[0]->stu_number,
                    'stu_branch'     => $ret[0]->stu_branch,
                    'stu_phone'      => $ret[0]->stu_phone,
                    'stu_email'      => $ret[0]->stu_email,
					
						  ),
					 );


	        $this->load->view('student/regis_student', $data);

}
            // อัพเดตข้อมูลนักศึกษา
public function update_student(){
        
		$this->form_validation->set_rules('stu_password', 'stu_password', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_name', 'stu_name', 'required');			// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_address', 'stu_address', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_birthday', 'stu_birthday', 'required');	// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_university', 'stu_university', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_number', 'stu_number', 'required');			// ดัก ช่องว่าง
		$this->form_validation->set_rules('stu_branch', 'stu_branch', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_phone', 'stu_phone', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('stu_email', 'stu_email', 'required');	// ดัก ช่องว่าง
    if ($this->form_validation->run() == FALSE){
		$this->edit_student();
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";

              
    }else{
        
       
        $id = $this->input->post('id');		// สร้างตัวแปรรับค่า ID 
        $stu_password = $this->input->post('stu_password');
        $stu_name     = $this->input->post('stu_name');
        $stu_address  = $this->input->post('stu_address');
        $stu_birthday = $this->input->post('stu_birthday');
        $stu_university = $this->input->post('stu_university');
        $stu_number   = $this->input->post('stu_number');
        $stu_branch   = $this->input->post('stu_branch');
        $stu_phone    = $this->input->post('stu_phone');
        $stu_email    = $this->input->post('stu_email');
            $data = array(
                     'id' => $id,
                    'update' => array(
               
                'stu_password'  => $stu_password,
                'stu_name'      => $stu_name,
                'stu_address'   => $stu_address,
                'stu_birthday'  => $stu_birthday,
                'stu_university'=> $stu_university,
                'stu_number'    => $stu_number,
                'stu_branch'    => $stu_branch,
                'stu_phone'     => $stu_phone,
                'stu_email'     => $stu_email
                    )
            );

			$this->modelall->update_student($data);
            
		 echo"<script language=\"JavaScript\">";
         echo"alert('แก้ไขข้อมูลสำเร็จแล้ว')";
         echo"</script>";
         $dataSTU = $this->edit_student($data['id']);
         $data = array(
            "dataview" =>array(
                        'stu_id'         => $dataSTU[0]->stu_id,
                        'stu_password'   => $dataSTU[0]->stu_password,
                        'stu_name'       => $dataSTU[0]->stu_name,
                        'stu_address'    => $dataSTU[0]->stu_address,
                        'stu_birthday'   => $dataSTU[0]->stu_birthday,
                        'stu_university' => $dataSTU[0]->stu_university,
                        'stu_number'     => $dataSTU[0]->stu_number,
                        'stu_branch'     => $dataSTU[0]->stu_branch,
                        'stu_phone'      => $dataSTU[0]->stu_phone,
                        'stu_email'      => $dataSTU[0]->stu_email,
                        
                
                ),
                
            );
                    
                
                $this->load->view('student/student_main',$data);
                
        }
    
}


        //แก้ไขข้อมูลบริษัท
 public function edit_company($ids=''){
        $id = $this->input->post('id');              //$ids=''
        if (!empty($ids)) {
			$ret = $this->modelall->get_company($ids);
            return $ret; 
        } else {
			$ret = $this->modelall->get_company($id);
               }
        
			
		$data = array(
		"dataview" =>array(
					'com_id'       => $ret[0]->com_id,            
                    'com_password' => $ret[0]->com_password,
					'com_name'     => $ret[0]->com_name,
					'com_address'  => $ret[0]->com_address,
					'com_web'      => $ret[0]->com_web,
                    'com_guid'     => $ret[0]->com_guid,
                    'com_phone'    => $ret[0]->com_phone,
                    'com_email'    => $ret[0]->com_email,
					
						  ),
					 );
// var_dump($data);

	$this->load->view('company/regis_company', $data);

}
// update บริษัท
public function update_company()
    {
		$this->form_validation->set_rules('com_password', 'com_password', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('com_name', 'com_name', 'required');			// ดัก ช่องว่าง
		$this->form_validation->set_rules('com_address', 'com_address', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('com_web', 'com_web', 'required');	// ดัก ช่องว่าง
		$this->form_validation->set_rules('com_guid', 'com_guid', 'required');		// ดัก ช่องว่าง
        $this->form_validation->set_rules('com_phone', 'com_phone', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('com_email', 'com_email', 'required');	// ดัก ช่องว่าง
    if ($this->form_validation->run() == FALSE){
		$this->edit_company();
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";

              
    }else{
        
       
        $id = $this->input->post('id');		// สร้างตัวแปรรับค่า ID 
        $com_password = $this->input->post('com_password');
        $com_name     = $this->input->post('com_name');
        $com_address  = $this->input->post('com_address');
        $com_web      = $this->input->post('com_web');
        $com_guid     = $this->input->post('com_guid');
        $com_phone    = $this->input->post('com_phone');
        $com_email    = $this->input->post('com_email');
            $data = array(
        'id'     => $id,
        'update' => array(
                'com_password'  => $com_password,
                'com_name'      => $com_name,
                'com_address'   => $com_address,
                'com_web'       => $com_web,
                'com_guid'      => $com_guid,
                'com_phone'     => $com_phone,
                'com_email'     => $com_email
                    )
            );

			$this->modelall->update_company($data);
		 echo"<script language=\"JavaScript\">";
         echo"alert('แก้ไขข้อมูลสำเร็จแล้ว')";
         echo"</script>";
         $dataSTU = $this->edit_company($data['id']);
         $data = array(
            "dataview" =>array(
                        'com_id'       => $dataSTU[0]->com_id,
                        'com_password' => $dataSTU[0]->com_password,
                        'com_name'     => $dataSTU[0]->com_name,
                        'com_address'  => $dataSTU[0]->com_address,
                        'com_web'      => $dataSTU[0]->com_web,
                        'com_guid'     => $dataSTU[0]->com_guid,
                        'com_phone'    => $dataSTU[0]->com_phone,
                        'com_email'    => $dataSTU[0]->com_email,
                ),
            );
            $this->load->view('company/company_main',$data);
        }
    
}


        // แก้ไขข้อมูล University
    public function edit_university($ids=''){
        $id = $this->input->post('id');              //$ids=''
        if (!empty($ids)) {
			$ret = $this->modelall->get_university($ids);
            return $ret; 
        } else {
			$ret = $this->modelall->get_university($id);
               }
        
			
		$data = array(
		"dataview" =>array(
					'uni_id'       => $ret[0]->uni_id,
                    'uni_password' => $ret[0]->uni_password,
					'uni_name'     => $ret[0]->uni_name,
                    'uni_teacher'     => $ret[0]->uni_teacher,
					'uni_address'  => $ret[0]->uni_address,
					'uni_web'      => $ret[0]->uni_web,
                    'uni_email'    => $ret[0]->uni_email,
					
						  ),
					 );

	$this->load->view('university/regis_university', $data);

    }
    //อัพเดตข้อมูล university
public function update_university(){

		$this->form_validation->set_rules('uni_password', 'uni_password', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('uni_teacher', 'uni_teacher', 'required');	
        $this->form_validation->set_rules('uni_name', 'uni_name', 'required');		// ดัก ช่องว่าง
		$this->form_validation->set_rules('uni_address', 'uni_address', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('uni_web', 'uni_web', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('uni_email', 'uni_email', 'required');	// ดัก ช่องว่าง
    if ($this->form_validation->run() == FALSE){
		$this->edit_university();
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";

              
    }else{
        
       
        $id = $this->input->post('id');		// สร้างตัวแปรรับค่า ID 
        $uni_password = $this->input->post('uni_password');
        $uni_name     = $this->input->post('uni_name');
         $uni_teacher     = $this->input->post('uni_teacher');
        $uni_address  = $this->input->post('uni_address');
        $uni_web      = $this->input->post('uni_web');
        $uni_email    = $this->input->post('uni_email');
            $data = array(
        'id'     => $id,
        'update' => array(
                'uni_password'  => $uni_password,
                'uni_name'      => $uni_name,
                'uni_teacher'      => $uni_teacher,
                'uni_address'   => $uni_address,
                'uni_web'       => $uni_web,
                'uni_email'     => $uni_email
                    )
            );

			$this->modelall->update_university($data);
		 echo"<script language=\"JavaScript\">";
         echo"alert('แก้ไขข้อมูลสำเร็จแล้ว')";
         echo"</script>";
         $dataSTU = $this->edit_university($data['id']);
         $data = array(
            "dataview" =>array(
                        'uni_id'       => $dataSTU[0]->uni_id,
                        'uni_password' => $dataSTU[0]->uni_password,
                        'uni_name'     => $dataSTU[0]->uni_name,
                        'uni_teacher'     => $dataSTU[0]->uni_teacher,
                        'uni_address'  => $dataSTU[0]->uni_address,
                        'uni_web'      => $dataSTU[0]->uni_web,
                        'uni_email'    => $dataSTU[0]->uni_email,
                ),
            );
            
            $this->load->view('university/university_main',$data);
        }
    
    }

    // หน้า main บริษัท  สร้างตำแหน่ง 
    public function position_create() {
         $this->form_validation->set_rules('pos_company', 'pos_company', 'required');
         $this->form_validation->set_rules('pos_name', 'pos_name', 'required');
         $this->form_validation->set_rules('pos_date', 'pos_date', 'required');

         
          if ($this->form_validation->run() == FALSE)    
		{
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";
                         $this->load->view('company/company_main');
              
        }else{

        $com_id_position = $this->input->post('pos_company');
        $pos_name = $this->input->post('pos_name');
        $pos_date = $this->input->post('pos_date');
         $data = array (
             'com_id_position'  => $com_id_position,
             'pos_name'  => $pos_name,
             'pos_date'  => $pos_date,
         
         );

            $this->modelall->insertposition($data);
            
            $this->load->view('company/company_main');
              }
             
    }
    // หน้า main student เลือกตำแหน่ง
    public function position_choose(){

        $data ['query'] = $this->modelall->list_position();
        $this->load->view('student/status_stu',$data);
        
    }
    // หน้า student/list_position  เลือกตำแหน่ง
    public function enroll_position() {
        
        $this->form_validation->set_rules('starttime', 'starttime', 'required');	// ดัก ช่องว่าง
        $this->form_validation->set_rules('endttime', 'endttime', 'required');	// ดัก ช่องว่าง
    
        if ($this->form_validation->run() == FALSE)
            {
                echo"<script language=\"JavaScript\">";
                echo"alert('กรุณากรอกวันที่เริ่มฝึกและวันที่ฝึกเสร็จให้ครบถ้วน')";
                echo"</script>";
                 $this->position_choose();
              
            }else{
                
                $stu_id = $this->input->post('stu_id');
                $pos_company = $this->input->post('pos_company');
                $pos_name = $this->input->post('pos_name');
                $stu_status = $this->input->post('stu_status');
                $starttime = $this->input->post('starttime');
                $endttime = $this->input->post('endttime');

                    $data = array (
                        'stu_id'  => $stu_id,
                        'position_stu_company'=> $pos_company,
                        'position_stu_name'=> $pos_name,
                        'stu_status'  =>  $stu_status,
                        'position_stu_starttime'=> $starttime,
                        'position_stu_endtime'=> $endttime,
                        
            );
            
            $this->modelall->insertstudent_position($data);
            echo"<script language=\"JavaScript\">";
            echo"alert('สมัครงานสำเร็จ')";
            echo"</script>";
           $this->load->view('student/student_main');
                 }
    }
    // ออกจากระบบ session
    public function logout(){
     
        session_destroy();
         echo"<script language=\"JavaScript\">";
         echo"alert('ออกจากระบบสำเร็จ')";
         echo"</script>";
        header("Location: index");
    }
    //เรียกหน้า mian student
    public function student_main(){
         
            $this->load->view('student/student_main');
    }
//เรียกหน้า mian company
    public function company_main(){
          
            $this->load->view('company/company_main');
    }
    //เรียกหน้า mian university
    public function university_main(){
           
            $this->load->view('university/university_main');
    }
    //หน้า main student ดูตำแหน่งที่สมัคร
    public function position_view() {

        $id = $this->input->get('id');
        $data = $this->modelall->list_status($id);
        $view = array('data'=> $data);
        $this->load->view('student/list_position',$view);
    }
    // หน้า main company เรียกดูรายชื่อผู้สมัคร
    public function list_RegisPosition(){
         
         $id = $this->input->get('id');
         $data = $this->modelall->list_RegisPosition($id);
         $view = array('data'=> $data);
         $this->load->view('company/list_RegisPosition',$view);
    }
        //หน้า  student/list_position  ยกเลิกตำแหน่งที่สมัคร
    public function deleted_regisposition(){

        $id = $this->input->get('id');
        $data = $this->modelall->deleted_regisposition($id);
        $this->position_view(); 
    }
        // บันทึกประจำวัน ของนักศึกษา -----------------------------------------------------ใหม่------------------------------------
    public function notes_student(){          
         
                $this->form_validation->set_rules('enr_enroll', 'enr_enroll', 'required');
                $this->form_validation->set_rules('enr_time', 'enr_time', 'required');

                if ($this->form_validation->run() == FALSE)    
		{
        echo"<script language=\"JavaScript\">";
        echo"alert('กรุณากรอกข้อมูลให้ครบถ้วน')";
        echo"</script>";
                         $this->load->view('student/student_main');
              
        }else{
                $enr_stu_id = $this->input->post('student');
                $enr_time   = $this->input->post('enr_time');
                $enr_enroll = $this->input->post('enr_enroll');
                    $data = array(
                        'enr_enroll' => $enr_enroll,
                        'enr_time'   => $enr_time,
                        'enr_stu_id' => $enr_stu_id,

                    );
                    $this->modelall->edit_notes_student($data);
                    // $this->load->view('student/student_main');
        }
            $this->load->view('student/student_main');
    }


        // หน้า main company จัดการตำแหน่งที่สร้าง
    public function edit_position(){
         
         $id = $this->input->get('id');
         $data = $this->modelall->edit_position($id);
         $view = array('data'=> $data);
          $this->load->view('company/edit_position',$view);
    }
    //หน้า company/list_RegisPosition  ยืนยันการสมัครของนักศึกษา
    public function verify_regisposition(){
        
        $company_id = $this->input->post('company_id');
        $position_id = $this->input->post('position_id');
        $student = $this->input->post('student');
        $position = $this->input->post('position');
        $stu_status = $this->input->post('status');
        
        $data = array (
                'che_company_id' => $company_id,
                'che_student'  => $student,
                'che_position' => $position,
            );       
        $this->modelall->verify_regisposition($data);
        if(empty($stu_status)){
            $status = array (
        'id'     => $student,
        'update' => array(
               
                'stu_status' => $stu_status,
                    )
            ); 
                $this->modelall->update_status($status);
                $this->company_main();
        }
    }
    //หน้า company/edit_position  ลบตำแหน่งที่สมัคร
    public function deleted_position(){
        
        $id = $this->input->get('id');
        $data = $this->modelall->deleted_position($id);
        $this->load->view('company/company_main');
    }

    //หน้า  company/edit_position  แก้ไขตำแหน่งที่สมัคร
    public function get_position(){

          $id = $this->input->get('id');
          $data = $this->modelall->get_position($id);
          $view = array('data'=> $data);     
       $this->load->view('company/update_position',$view);
    }
    public function list_student_com(){
        
          $id = $this->input->get('id');
          $data = $this->modelall->list_student_com($id);
          $view = array('data'=> $data);     
       $this->load->view('company/list_student_com',$view);
    }
    public function stu_get_university()
    {
        $name = $this->input->post('university');
        $data = $this->modelall->get_name_university($name);
        $view = array('data'=> $data);    
       $this->load->view('student/stu_choose_university',$view);
    }
    public function stu_choose_university()
    {
       $id = $this->input->get('id');
       $view = array("data"=> $id); 
       $this->load->view('student/regis_student',$view);

    }
    public function uni_get_university()
    {
        $name = $this->input->post('university');
        $data = $this->modelall->get_name_university($name);
        $view = array('data'=> $data);    
       $this->load->view('university/uni_choose_university',$view);
    }
        public function uni_choose_university()
    {
       $id = $this->input->get('id');
       $view = array("data"=> $id); 
       $this->load->view('university/regis_university',$view);

    }
    // all_enroll  ดูบันทึกนักศึกษาทั้งหมด ----------------------------------------------------------------------------------------
    public function all_enroll(){
        $id = $this->input->get('id');
        $data = $this->modelall->all_stu_enroll($id);
        $view = array('data'=> $data);
        $this->load->view('student/list_allenroll',$view);
    }
    // all_enroll  ดูบันทึกนักศึกษาทั้งหมด  ของบริษัท-----------------------------------------------------------------------------------
    public function all_enroll_com(){
        
        $id = $this->input->get('id');
        $data = $this->modelall->all_stu_enroll_com($id);
        $view = array('data'=> $data);
        $this->load->view('company/list_allenroll_com',$view);
        
    }
    // เรียกดูชื่อนักศึกษาที่อยู่ในมหาวิทยาลัย
    public function list_stu_university()
    {
        $id = $this->input->get('id');
        $data = $this->modelall->list_stu_university($id);
        $view = array('data'=> $data);
       $this->load->view('university/list_stu_university',$view);

    }
    // บริษัทดูบันทึกนักศึกษา company
    // public function read_note()
    // {
    //     $id = $this->input->get('id');
    //     $data = $this->modelall->read_note($id);
    //     $view = array('data'=> $data);    
    //     $this->load->view('company/read_note',$view);
    // }
    // บริษัทดูบันทึกนักศึกษา university
    public function read_note_uni(){

        $id = $this->input->get('id');
        $data = $this->modelall->read_note($id);
        $view = array('data'=> $data);    
        $this->load->view('university/read_note_uni',$view);
    }
    // เช็คชื่อ check_student  
    public function check_student_com(){

        $id = $this->input->get('id');
        $data = $this->modelall->check_student_com($id);
        $view = array('data'=> $data);
        $this->load->view('company/check_student',$view);
    }
    
    public function list_check_student(){

        $stu_name = $this->input->post('stu_name');
        $com_id_position = $this->input->post('com_id_position');
        $gender = $this->input->post('gender');
            $data = array(
                'lis_stu_id' => $stu_name,
                'lis_com_id' => $com_id_position,
                'lis_check'  => $gender,
            );
            // var_dump($data);
            $this->modelall->list_check_student($data);
            $this->load->view('company/company_main');
    }
}
    