<html>
    <head>
                <title>เรียกดูรายชื่อผู้สมัคร</title>
                <link rel="stylesheet"  href="/trainee/assets/css/list_RegisPosition.css">
    </head>
        <body>
            

            <div class="img">
                <a href ="<?php echo base_url("index.php/controller/company_main")?>"><img src="/trainee/assets/img/home.png" width="70px" height="70px"></a>
            </div>
   
            <?php if (isset($data)): ?>
                <div class="centerbody">
                    <?php foreach($data as $r):?>
                        <div class="boxjob">
                            
                                
                                <p><b>ผู้สมัคร :<?php echo $r->stu_name; ?></b></p>
                                <p><b>ที่อยู่ :<?php echo $r->stu_address; ?></b></p>
                                <p><b>มหาวิทยาลัย :<?php echo $r->stu_university; ?></b></p>
                                <p><b>สาขาวิชา :<?php echo $r->stu_branch; ?></b></p>
                                <p><b>เบอร์โทร :<?php echo $r->stu_phone; ?></b></p>
                                <p><b>อีเมล :<?php echo $r->stu_email; ?></b></p>
                                <p><b>สมัครตำแหน่ง :<?php echo $r->pos_name; ?></b></p>
                                <p><b>เวลาเริ่มฝึก :<?php echo $r->position_stu_starttime; ?></b></p>
                                <p><b>เวลาสิ้นสุดการฝึก :<?php echo $r->position_stu_endtime; ?></b></p>

                                <?php echo form_open('controller/verify_regisposition');?>
                                <input type="hidden" name="company_id" value="<?php echo $this->session->userdata('com_id');?>">
                                <input type="hidden" name="position_id" value="<?php echo $r->position_stu_number; ?>">
                                <input type="hidden" name="student" value="<?php echo $r->stu_id; ?>">
                                <input type="hidden" name="position" value="<?php echo $r->position_stu_name; ?>">
                                <input type="hidden" name="status" value="0">
                                <button type="submit" name="submit" OnClick="return chkdel();">ยอมรับการสมัคร</button>
                                
                                <script language="JavaScript">
                                    function chkdel(){if(confirm('  กรุณายืนยันการยอมรับการสมัครอีกครั้ง !!!  ')){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                    }
                                    </script -->
                                <?php echo form_close();?>
                                
                            
                        </div>
                    </tr>
                    
                    <?php endforeach; ?>
                
                    
                </div> 
            <?php endif; ?>
            
            </div>
    </body>
        
        <style type="text/css"> 
            #MD-StoTop {-moz-border-radius: 5px;-webkit-border-radius: 5px;
            border-radius: 5px; 
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#99EEEEEE',EndColorStr='#99EEEEEE');
            position:fixed;
            bottom:60px;
            right:60px;
            cursor:pointer;
            text-decoration:none;
            alpha:(opacity=20);
            opacity: 0.3;   
        </style>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 

        <script type='text/javascript'> 
            $(function() {     
                $.fn.scrollToTop = function() {     
                    $(this).hide().removeAttr("href"); 

                    if ($(window).scrollTop() != "0") 
                    {         
                        $(this).fadeIn("slow")     
                    }     
                        
                        var scrollDiv = $(this);

                    $(window).scroll(function() 
                    {         
                        if ($(window).scrollTop() == "0") 
                        {         
                            $(scrollDiv).fadeOut("slow")         
                        } 
                        else 
                        {         
                            $(scrollDiv).fadeIn("slow")         
                        }
                    }); 

                    $(this).click(function() 
                    {         
                        $("html, body").animate({scrollTop: 0}, "slow")     })     } }); 
                        $(function() { $("#MD-StoTop").scrollToTop(); }); 
                                        
        </script> 
            <a href='#' id='MD-StoTop' style='display:none;'><img src="/trainee/assets/img/up.png" width="40px" height="40px"></a>

</html>