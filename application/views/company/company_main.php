<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agency - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href=<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.css')?> rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css')?> rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href=<?php echo base_url('assets/css/agency.css')?> rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Welcome Company</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">ประวัติบริษัท</a>
                    </li>
                    <li>
                        <a class="page-scroll"  href="<?php echo base_url('index.php/controller/list_student_com');?>?id=<?php echo $this->session->userdata('com_id');?>">ผู้ฝึกงานในบริษัท</a>
                    </li>
                    <li>
                        <a class="page-scroll"  href="<?php echo base_url('index.php/controller/list_RegisPosition');?>?id=<?php echo $this->session->userdata('com_id');?>">รายชื่อผู้สมัคร</a>
                    </li>
                    <li>
                        <a class="page-scroll"  href="<?php echo base_url('index.php/controller/edit_position');?>?id=<?php echo $this->session->userdata('com_id');?>">แก้ไขตำแหน่ง</a>
                    </li>
                    <li>
                        <a class type="submit" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">สร้างตำแหน่ง</a>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content"> 
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel">สร้างตำแหน่งงาน</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <?php echo form_open('controller/position_create');?>        
                                    <input type="hidden" name="pos_company" value="<?php echo $this->session->userdata('com_id');?>" ><ul>
                                    <ul><label for="recipient-name" class="control-label">ตำแหน่งรับนักศึกษา:</label>  
                                        <input type="text" class="form-control" name="pos_name"><ul>
                                            <ul><label for="recipient-name" class="control-label">วันสิ้นสุดการรับ:</label>  
                                        <input type="date" class="form-control" name="pos_date"><ul>
                                        <br><button type="submit" class="btn btn-primary">สร้างตำแหน่ง</button></br>
                                        <?php echo form_close();?>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </li>
                    <li>
                        <a class="page-scroll"  href="<?php echo base_url('index.php/controller/check_student_com');?>?id=<?php echo $this->session->userdata('com_id');?>">เช็คชื่อ</a>
                    </li>
                    <li>
                        <a class="page-scroll" href=<?php echo base_url('index.php/controller/logout'); ?> OnClick="return chkdel();">ออกจากระบบ</a>
                        <script language="JavaScript">
                                    function chkdel(){if(confirm('  กรุณายืนยันการออกจากระบบอีกครั้ง !!!  ')){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                    }
                                    </script -->
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">ยินดีต้อนรับ ทางบริษัท</div>
                <div class="intro-heading">"Welcome Company" ?</div>

            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Profile</h2>
                    <h3 class="section-subheading text-muted">ประวัติส่วนตัว</h3>
                    
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                     </span>
                     <?php  if (!empty($dataview)) {?>
                     <?php echo form_open('controller/edit_company');?>
                     <input class="hidden" type="text" name="id" value="<?php echo $this->session->userdata('com_id');?>"/>
                    <h4 class="service-heading"><br><b>ชื่อบริษัท</b></h4> <p><?php echo $dataview['com_name'];?></p></br>
                    <p class="text-muted"><br><b><h4>ที่อยู่บริษัท</h4></b><?php echo $dataview['com_address'];?></p></br>
                    <p class="text-muted"><br><b><h4>เว็บไซต์บริษัท</h4></b><?php echo $dataview['com_web'];?></p></br>
                    <p class="text-muted"><br><b><h4>ชื่อผู้ดูแลนักศึกษา</h4></b> <?php echo $dataview['com_guid'];?></p></br>
                    <p class="text-muted"><br><b><h4>เบอร์โทร</h4></b> <?php echo $dataview['com_phone'];?></p></br>
                    <p class="text-muted"><br><b><h4>อีเมล</h4></b> <?php echo $dataview['com_email'];?></p></br>
                    <br><p> <button type="submit" name="edit">แก้ไขข้อมูลส่วนตัว</button></p></br>
                    <?php echo form_close();?>
                </div>
        </div>
                <?php } else { ?>
                <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                     </span>
                     
                     <?php echo form_open('controller/edit_company');?>
                     <input class="hidden" type="text" name="id" value="<?php echo $this->session->userdata('com_id');?>"/>
                    <h4 class="service-heading"><br><b>ชื่อบริษัท</b></h4> <p><?php echo $this->session->userdata('com_name');?></p></br>
                    <p class="text-muted"><br><b><h4>ที่อยู่บริษัท</h4></b><?php echo $this->session->userdata('com_address');?></p></br>
                    <p class="text-muted"><br><b><h4>เว็บไซต์บริษัท</h4></b><?php echo $this->session->userdata('com_web');?></p></br>
                    <p class="text-muted"><br><b><h4>ชื่อผู้ดูแลนักศึกษา</h4></b> <?php echo $this->session->userdata('com_guid');?></p></br>
                    <p class="text-muted"><br><b><h4>เบอร์โทร</h4></b> <?php echo $this->session->userdata('com_phone');?></p></br>
                    <p class="text-muted"><br><b><h4>อีเมล</h4></b> <?php echo $this->session->userdata('com_email');?></p></br>
                    <br><p> <button type="submit" name="edit">แก้ไขข้อมูลส่วนตัว</button></p></br>
                    <?php echo form_close();?>
                    <?php }?>
                </div>
        </div>

    </section>



    <!--footter-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Student Record 2017</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Go top</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->


    <!-- jQuery -->
    <script src=<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js')?>></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src=<?php echo base_url('assets/js/jqBootstrapValidation.js')?>></script>
    <script src=<?php echo base_url('assets/js/contact_me.js')?>></script>

    <!-- Theme JavaScript -->
    <script src=<?php echo base_url('assets/js/agency.min.js')?>></script>

</body>

</html>
