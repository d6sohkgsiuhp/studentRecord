<html>
    <head>
        <title>เลือกมหาวิทยาลัย</title>
        <link rel="stylesheet"  href="/trainee/assets/css/choose_university.css">         
    </head>

        <body>
            <?php echo form_open('controller/stu_get_university');?>
                <div class="search">
                    <div class="img">
                        <a href ="<?php echo base_url("index.php/controller/index")?>"><img src="/trainee/assets/img/home.png" width="70px" height="70px"></a>
                    </div>
                    <input type="text" name="university">
                    <button type="submit">ค้นหามหาวิทยาลัย</button>
                </div>
            <?php echo form_close();?>

                <?php if (isset($data)): ?>
                    <div class="centerbody">
                        <?php foreach($data as $r):?>
                            <div class="boxjob">
                                <p><b>ชื่อมหาวิทยาลัย :</b><?php echo $r->nam_name; ?></p>
                                <a href="<?php echo base_url('index.php/controller/stu_choose_university')?>?id=<?php echo $r->nam_name; ?>">สมัคร</a>  
                            </div>                
                        <?php endforeach; ?>   
                    </div>
                <?php endif; ?>
        </body>
         
        <style type="text/css"> 
            #MD-StoTop {-moz-border-radius: 5px;-webkit-border-radius: 5px;
            border-radius: 5px; 
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#99EEEEEE',EndColorStr='#99EEEEEE');
            position:fixed;
            bottom:60px;
            right:60px;
            cursor:pointer;
            text-decoration:none;
            alpha:(opacity=20);
            opacity: 0.3;   
        </style>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 

        <script type='text/javascript'> 
            $(function() {     
                $.fn.scrollToTop = function() {     
                    $(this).hide().removeAttr("href"); 

                    if ($(window).scrollTop() != "0") 
                    {         
                        $(this).fadeIn("slow")     
                    }     
                        
                        var scrollDiv = $(this);

                    $(window).scroll(function() 
                    {         
                        if ($(window).scrollTop() == "0") 
                        {         
                            $(scrollDiv).fadeOut("slow")         
                        } 
                        else 
                        {         
                            $(scrollDiv).fadeIn("slow")         
                        }
                    }); 

                    $(this).click(function() 
                    {         
                        $("html, body").animate({scrollTop: 0}, "slow")     })     } }); 
                        $(function() { $("#MD-StoTop").scrollToTop(); }); 
                                        
        </script> 
            <a href='#' id='MD-StoTop' style='display:none;'><img src="/trainee/assets/img/up.png" width="40px" height="40px"></a>

</html>