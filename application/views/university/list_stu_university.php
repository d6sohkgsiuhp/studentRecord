<html>
    <head>
        <title>เลือกมหาวิทยาลัย</title>
        <link rel="stylesheet"  href="/trainee/assets/css/list_stu_university.css">        
    </head>

        <body>
            <div class="img">
                <a href ="<?php echo base_url("index.php/controller/university_main")?>"><img src="/trainee/assets/img/home.png" width="70px" height="70px"></a>
            </div>

                <?php if (isset($data)): ?>
                    <div class="centerbody">
                        <?php foreach($data as $r):?>
                            <div class="boxjob">
                                         <p><b>ชื่อนักศึกษา :</b><?php echo $r->stu_name; ?></p>
                                         <p><b>ที่อยู่ :</b><?php echo $r->stu_address; ?></p>
                                         <p><b>วัน/เดือน/ปีเกิด :</b><?php echo $r->stu_birthday; ?></p>
                                         <p><b>มหาวิทยาลัย :</b><?php echo $r->stu_university; ?></p>
                                         <p><b>รหัสนักศึกษา :</b><?php echo $r->stu_number; ?></p>
                                         <p><b>สาขาวิชา :</b><?php echo $r->stu_branch; ?></p>
                                         <p><b>ฝึกประสบการณืวิชาชีพที่ :</b><?php echo $r->com_name; ?></p>
                                         <p><b>ตำแหน่งที่ฝึก :</b><?php echo $r->pos_name; ?></p>
                                         <p><b>เบอร์โทร :</b><?php echo $r->stu_phone; ?></p>
                                         <p><b>อีเมล :</b><?php echo $r->stu_email; ?></p>  
                                         <a href="<?php echo base_url('index.php/controller/read_note_uni')?>?id=<?php echo $r->stu_id; ?>">ดูบันทึก</a>
                            </div>
                        <?php endforeach; ?>  
                    </div> 
                <?php endif; ?>
        </body>
         
        <style type="text/css"> 
            #MD-StoTop {-moz-border-radius: 5px;-webkit-border-radius: 5px;
            border-radius: 5px; 
            filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#99EEEEEE',EndColorStr='#99EEEEEE');
            position:fixed;
            bottom:60px;
            right:60px;
            cursor:pointer;
            text-decoration:none;
            alpha:(opacity=20);
            opacity: 0.3;   
        </style>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 

        <script type='text/javascript'> 
            $(function() {     
                $.fn.scrollToTop = function() {     
                    $(this).hide().removeAttr("href"); 

                    if ($(window).scrollTop() != "0") 
                    {         
                        $(this).fadeIn("slow")     
                    }     
                        
                        var scrollDiv = $(this);

                    $(window).scroll(function() 
                    {         
                        if ($(window).scrollTop() == "0") 
                        {         
                            $(scrollDiv).fadeOut("slow")         
                        } 
                        else 
                        {         
                            $(scrollDiv).fadeIn("slow")         
                        }
                    }); 

                    $(this).click(function() 
                    {         
                        $("html, body").animate({scrollTop: 0}, "slow")     })     } }); 
                        $(function() { $("#MD-StoTop").scrollToTop(); }); 
                                        
        </script> 
            <a href='#' id='MD-StoTop' style='display:none;'><img src="/trainee/assets/img/up.png" width="40px" height="40px"></a>

</html>