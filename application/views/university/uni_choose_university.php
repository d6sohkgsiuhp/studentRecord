<html>
    <head>
        <title>เลือกมหาวิทยาลัย</title>
        <link rel="stylesheet"  href="/trainee/assets/css/choose_university.css">        
    </head>

        <body>
            <?php echo form_open('controller/uni_get_university');?>
            <div class="search">
                <div class="img">
                        <a href ="<?php echo base_url("index.php/controller/index")?>"><img src="/trainee/assets/img/home.png" width="70px" height="70px"></a>
                </div>
                <input type="text" name="university">
                <button type="submit">ค้นหามหาวิทยาลัย</button>
            </div>
            <?php echo form_close();?>

                <?php if (isset($data)): ?>
                    <div class="centerbody">
                        <?php foreach($data as $r):?>
                            <div class="boxjob">
                                <p><b>ชื่อมหาวิทยาลัย :</b><?php echo $r->nam_name; ?></p>
                                <a href="<?php echo base_url('index.php/controller/uni_choose_university')?>?id=<?php echo $r->nam_name; ?>">สมัคร</a>
                            </div>                  
                        <?php endforeach; ?>    
                    </div>       
                <?php endif; ?>
        </body>
</html>